import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
    apiKey: "AIzaSyAODOa1VpV4S0FXe6RtIRL1YbBCcXEdz2c",
    authDomain: "tp-foro.firebaseapp.com",
    projectId: "tp-foro",
    storageBucket: "tp-foro.appspot.com",
    messagingSenderId: "1041030864258",
    appId: "1:1041030864258:web:a483b209e2cd3c8fc7039b"
  };

  const app = initializeApp(firebaseConfig);
  const auth = getAuth();

// Referencias al HTML --- Login 

let correoRefLog = document.getElementById("Email-Login");
let passRefLog = document.getElementById("Password-Login");
let buttonRefLog = document.getElementById("Boton-Login");

buttonRefLog.addEventListener("click", LogIn);


// funcion para Iniciar sesion

function LogIn (){

    if((correoRefLog.value != '') && (passRefLog.value != '')){

        signInWithEmailAndPassword(auth, correoRefLog.value, passRefLog.value)
        .then((userCredential) => {
            const user = userCredential.user;
            window.location.href = "../index.html";
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    

}
